# install puppet agent
# wget https://apt.puppetlabs.com/puppet6-release-bionic.deb
# sudo dpkg -i puppet6-release-bionic.deb
# sudo apt update
# sudo apt install puppet-agent
# sudo ln -s /opt/puppetlabs/bin/puppet /usr/bin

#install modules
# sudo puppet module install puppetlabs-java
# sudo puppet module install puppetlabs-nginx
# sudo puppet module install puppetlabs-apt

$svc_conf_template = @(END)
[Unit]
Description=Service for the Investimentos API

[Service]
ExecStart=/usr/bin/java -jar /home/ubuntu/Api-Investimentos-0.0.1-SNAPSHOT.jar

[Install]
WantedBy=multi-user.target
END

# instalar o java, especificamente pacote 'openjdk-8-jdk'
# exemplo na documentação do módulo
class { 'java' :
  package => 'openjdk-8-jdk',
}

file { '/etc/systemd/system/meuservico.service':
  ensure => file,
  content => inline_epp($svc_conf_template),
}

# gerenciar o tipo de recurso correspondente ao service acima e garantir que está rodando
service {'meuservico':
  ensure => stopped
}

# configurar o nginx (instalação e proxy reverso)
# (tem o exemplo na documentação do módulo no forge.puppet.com)
class { 'nginx' : }
nginx::resource::server { 'localhost':
  listen_port => 80,
  proxy       => 'http://localhost:8090',
}

file { "/etc/nginx/conf.d/default.conf":
 ensure => absent,
 notify => Service['nginx'],
}

ssh_authorized_key { 'ton.unesp@gmail.com':
  ensure => present,
  user   => 'ubuntu',
  type   => 'ssh-rsa',
  key    => 'AAAAB3NzaC1yc2EAAAADAQABAAACAQCwsBU7H97yRXEP2qEPxTVnR3ANTIYyqykCdv+yDvuEzZVWWc3BAEsVYrqMMsXImsYbZSqTmp8+kQ4AXLZAMvD2VBZMnSs8UjTFZL0juqA64JhLcXge/mNAA5ym2/1HIbW5tOl/+1ifiM4iCuHU64K/c4rqMKfdr6cYd6tBb9Y2wQ/bpBweFrvTm8xDh/qavgFzrgx2Dh3mZSSmuNzn6Z9S/pUJLtM2Nfvzwn8IFo2pXkbQoLUv9UtXciNL1zvqNqWlLt6M+SM0lqrvWL53CWqB1whvmDGPkE45EfGqx5AO42YyB+OAKuI3I91XUF/amM31SjSoqh0efTFmxKPFWX9NyQ8xg/0P2DHnhCxuI6ESL+fA/iQ1B+qjqaO+y5jcG/eh21nx90zX4Ac+aydOy24v7PVZyMYBMbBtpbi4VQVkanAll5brZwltHYZty12f1GeGqac+JGUEJOpYUncja9bpP/Eo4+s5LtLI0Pqs9ayhEXSndY2vBg24O8cz5IPb7WhIGzar8CZ0K3qVr84W76D792yzNNfuek7Mr+MPTyYOYbzN+Y5vRPnu+rBrXM7++cyfpwaD4oVYr2y/v9BD/+1bB1cV+ASLwUR6DJotfweP2Om6qC1woC7uGCC+p1zI3XOanq6/7Po7vTccfGOunFTMnec3T03qUxlOgg0RMxnD1Q==',
}

ssh_authorized_key { "jenkins@banana":
  ensure => present,
  user => "ubuntu",
  type => "ssh-rsa",
  key => "AAAAB3NzaC1yc2EAAAADAQABAAABAQDvNfe5q8SfXeGyl6cB2vR4mo7pHq30sjPgMrhP05UF2gH3bgz7dGREpb6caCGJo8gpQXofeEI0wdqLw0KbmIuqSMpT4yQQHWr6VqWbDaK2XQutHJkElGQMTCBADwT4bGt8b2eNV8s581g/RS01q4L7F9iSkMzSms3BM63FkZHZAOlOTHmRnN3Ju39MolmjGPwb6SpUFYpMIZLic1vKTe4p1L3PQL4czIJxpssE283iOeRiAceD/Xaly4mgVqdxmMEm3gZo5PzcPMu5Y08M6Ur0vJR5i4IVSSheBhzcKLshuvTBt5bobYtG1BWatQIpbKoCO6Uqnf/7U7FvPHJTYsJb",
}